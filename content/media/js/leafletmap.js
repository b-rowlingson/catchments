var map;
var ajaxRequest;
var plotlist;
var plotlayers=[];

function initmap(lat,long, postcode) {
	// set up the map
	map = new L.Map('map');

	// create the tile layer with correct attribution
	var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
	var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 19, attribution: osmAttrib});		

	// start the map in South-East England
	map.setView(new L.LatLng(lat, long),14);
    map.addLayer(osm);
    L.marker([lat, long],{'title': postcode}).addTo(map);
    L.circle([lat, long],{'radius': 1000, fill: false}).addTo(map);
}

function addgpmap(url){
    map = new L.Map('map');
    
    // create the tile layer with correct attribution
    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
    var osm = new L.TileLayer(osmUrl, {minZoom: 8, maxZoom: 19, attribution: osmAttrib});		
    
    // start the map here
    map.setView(new L.LatLng(54.2,-2.7),8);
    map.addLayer(osm);

    var pts = new L.geoJson([],{
	onEachFeature: function(feature, layer){
	    layer.bindPopup('<h1><a href="'+feature.properties.Code+'">'+feature.properties.Name+'</a></h1><p>'+feature.properties.Area+'</p>');
	}
    });
    pts.addTo(map);
    $.ajax({
	dataType: "json",
	url: url,
	success: function(data) {
	    $(data.features).each(function(key, data) {
		pts.addData(data);
	    });
	}
    }).error(function() {});
    
}
